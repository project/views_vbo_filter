(function ($) {

  /**
   * Adds functionality for our "Add filter values" link.
   */
  Drupal.behaviors.viewsVboFilter = {

    attach: function (context) {
      var $context = $(context);
      $context.find('.views-vbo-filter').once('views-vbo-filter', function () {
        var $link = $(this);
        var $filter = $link.closest('form').find('[name="' + $link.data('target-filter') + '"]');
        var href = $link.attr('href');
        var label = $link.text();
        var button_label = $link.data('button-label');
        var modal_title = $link.data('modal-title');
        $link.attr('href', '#');
        if (!$filter.length || !href) {
          return;
        }

        var view_dom_id = Math.random().toString(36).substr(2).toUpperCase();
        var view_dom_class = 'view-dom-id-' + view_dom_id;
        var $wrapper = $([]);

        // AJAXify the link.
        var element_settings = {
          event: 'click',
          progress: {type: 'throbber'},
          url: href + '&view_dom_id=' + view_dom_id,
        };
        var base = $link.attr('id');
        Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);

        // Add our own "success" handler to show the popup.
        var old_success = Drupal.ajax[base].options.success;
        Drupal.ajax[base].options.success = function () {
          // First, prepare the div that will contain the shown view (if
          // necessary).
          if (!$wrapper.length || !$('body .' + view_dom_class).length) {
            $wrapper.remove();
            $wrapper = $('<div style="display: none;" class="views-vbo-filter-modal-content-placeholder"></div>');
            $wrapper.data('view_ajax_url', element_settings.url);
            $('body').append($wrapper);
            $wrapper.append('<div class="' + view_dom_class + '"></div>');
          }

          // Then, execute Drupal's standard behavior, executing all the AJAX
          // commands contained in the response.
          old_success.apply(this, arguments);

          // Finally, show the popup with the view.
          Drupal.viewsVboFilter.showPopup($wrapper, $filter, modal_title, button_label);
        };

        $(document).bind('CToolsDetachBehaviors.views_vbo_filter', function () {
          // When the popup is closed, exit fullscreen mode (if applicable).
          Drupal.viewsVboFilter.exitFullscreen();
          // Also remove our (mobile-only) class indicating an active fulltext
          // modal.
          $('body').removeClass('views-vbo-filter-modal-fullscreen');
        });
      });

      // Fix for "Reset" buttons inside the modal.
      var $modal = $context.closest('.views-vbo-filter-modal-content, .views-vbo-filter-modal-content-placeholder');
      if ($modal.length && $modal.data('view_ajax_url')) {
        $context.find('.views-reset-button .form-submit').each(function (e) {
          var base = $(this).attr('id');
          var element_settings = {
            event: 'click',
            url: $modal.data('view_ajax_url'),
            setClick: true,
            progress: {type: 'throbber'},
          };
          Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
          Drupal.ajax[base].form = false;
        });
      }
    }

  };

  Drupal.viewsVboFilter = Drupal.viewsVboFilter || {};

  /**
   * Shows the "Add filter values" popup.
   */
  Drupal.viewsVboFilter.showPopup = function ($wrapper, $filter, title, button_label) {
    var modal_settings = {
      modalSize: {
        type: 'scale',
        width: 0.9,
        height: 0.98,
      },
      modalOptions: {
        opacity: 0.85,
        background: '#000',
      },
      modalTheme: 'viewsVboFilterModal',
      closeText: Drupal.t('Close'),
    };

    // On small displays, show the popup maximized.
    var $window = $(window);
    var small_display = $window.width() <= 640;
    if (small_display) {
      modal_settings.modalSize.width = 1;
      modal_settings.modalSize.height = 1;
      // Disable scrolling (rather hack-ish).
      $('body').addClass('views-vbo-filter-modal-fullscreen');
    }

    Drupal.CTools.Modal.show(modal_settings);

    // Add our title and content.
    var $modal = $('#modalContent');
    $modal.find('.modal-title').html(title);
    $modal.find('.views-vbo-filter-modal-content').data($wrapper.data());
    var $modal_content = $modal.find('.modal-content');
    $modal_content.empty().append($wrapper.children());

    // Add click handlers for our buttons.
    var $buttons = $modal.find('.views-vbo-filter-buttons');
    $buttons.find('.submit')
      .text(button_label)
      .click(function () {
        var values = [];
        var map = {};
        $modal_content.find('input.vbo-select:checked').each(function () {
          if (this.value) {
            values.push(this.value);
            map[this.value] = $(this).closest('.views-row');
          }
        });
        if (values.length) {
          $filter.triggerHandler('views-vbo-filter-add', [values, map]);
          Drupal.viewsVboFilter.addFilterValues($filter, values);
        }
        Drupal.CTools.Modal.dismiss();
        return false;
      });
    $buttons.find('.cancel')
      .click(function () {
        Drupal.CTools.Modal.dismiss();
        return false;
      });

    if (small_display) {
      // On small displays, show the popup maximized and in fullscreen mode (no
      // address bar).
      $modal.css('position', 'fixed');
      $modal.css('top', '0');
      $modal.css('left', '0');
      $window.unbind('resize scroll', modalContentResize);
      Drupal.viewsVboFilter.requestFullscreen();
    }
    else {
      // Otherwise, make the modal (width) fit its contents.
      $window.resize(Drupal.viewsVboFilter.fitModalToContents);
      Drupal.viewsVboFilter.fitModalToContents();
    }

    $window.triggerHandler('views-vbo-filter-modal-show', [$modal]);
  };

  /**
   * Enters fullscreen mode on mobile browsers.
   *
   * @see http://www.html5rocks.com/en/mobile/fullscreen/
   */
  Drupal.viewsVboFilter.requestFullscreen = function () {
    var doc = window.document;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
      var docEl = doc.documentElement;
      var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
      if (requestFullScreen) {
        requestFullScreen.call(docEl);
      }
    }
  };

  /**
   * Cancels fullscreen mode on mobile.
   *
   * @see http://www.html5rocks.com/en/mobile/fullscreen/
   */
  Drupal.viewsVboFilter.exitFullscreen = function () {
    var doc = window.document;

    if (doc.fullscreenElement || doc.mozFullScreenElement || doc.webkitFullscreenElement || doc.msFullscreenElement) {
      var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
      if (cancelFullScreen) {
        cancelFullScreen.call(doc);
      }
    }
  };

  /**
   * Window resize handler: Counteracts the resize handler of the CTools modal.
   */
  Drupal.viewsVboFilter.fitModalToContents = function () {
    var $modal = $('#modalContent');
    $modal.css({
      top: '1%',
      left: '50%',
      transform: 'translate(-50%, 0)',
      width: 'auto'
    });
    $modal.find('.views-vbo-filter-modal-content').css('width', 'auto');
    $modal.find('.modal-content').css('width', 'auto');
  };

  /**
   * Adds values to a filter.
   *
   * @param $filter
   *   A jQuery object representing a filter's input element.
   * @param values
   *   The values to add.
   */
  Drupal.viewsVboFilter.addFilterValues = function ($filter, values) {
    var val = $filter.val();
    var i;
    if (typeof val == 'string') {
      for (i in values) {
        if (val) {
          val += ',';
        }
        val += values[i];
      }
    }
    else if (typeof val == 'object' && val.constructor === Array) {
      for (i in values) {
        val.push(values[i]);
      }
    }
    $filter.val(val);
    $filter.change();
  };

  /**
   * Provide the HTML to create the modal dialog.
   */
  Drupal.theme.prototype.viewsVboFilterModal = function () {
    return '<div id="ctools-modal" class="popups-box">' +
        '  <div class="ctools-modal-content views-vbo-filter-modal-content">\n' +
        '    <div class="popups-container">\n' +
        '      <div class="modal-header popups-title">\n' +
        '        <span class="modal-title" class="modal-title"><\/span>\n' +
        '        <span class="views-vbo-filter-buttons">\n' +
        '          <button class="submit"></button>\n' +
        '          <button class="cancel">\n' +
        '            ' + Drupal.t('Cancel') +
        '          </button>\n' +
        '        <\/span>\n' +
        '        <div class="clear-block"><\/div>\n' +
        '      <\/div>\n' +
        '      <div class="modal-scroll"><div class="modal-content popups-body"><\/div><\/div>\n' +
        '    <\/div>\n' +
        '  <\/div>\n' +
        '<\/div>';
  };

})(jQuery);
