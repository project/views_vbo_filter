<?php

/**
 * @file
 * Contains ViewsVboFilter.
 */

/**
 * Provides a link opening a modal for adding values to another filter.
 */
class ViewsVboFilter extends views_handler_filter {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['exposed']['default'] = TRUE;
    $options['options_view']['default'] = NULL;
    $options['filter']['default'] = NULL;
    $options['expose']['contains']['button_label'] = array(
      'default' => 'Add values',
      'translatable' => TRUE,
    );
    $options['expose']['contains']['modal_title'] = array(
      'default' => '',
      'translatable' => TRUE,
    );

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    unset(
      $form['expose_button'],
      $form['expose']['required'],
      $form['expose']['multiple'],
      $form['expose']['remember'],
      $form['expose']['remember_roles']
    );

    $views_options = array();
    $views_data = views_fetch_data();
    foreach (views_get_all_views() as $view_id => $view) {
      foreach ($view->display as $display_id => $display) {
        if (isset($display->display_options['fields'])) {
          $fields = $display->display_options['fields'];
        }
        elseif ($display->id != 'default') {
          $fields = $view->display['default']->display_options['fields'];
        }
        else {
          $fields = array();
        }
        foreach ($fields as $field) {
          if (empty($views_data[$field['table']][$field['field']]['field']['handler'])) {
            continue;
          }
          if ($views_data[$field['table']][$field['field']]['field']['handler'] == 'views_bulk_operations_handler_field_operations') {
            $views_options["$view_id:$display_id"] = $view->human_name . ': ' . $display->display_title;
            break;
          }
        }
      }
    }
    asort($views_options, SORT_NATURAL | SORT_FLAG_CASE);
    $form['options_view'] = array(
      '#type' => 'select',
      '#title' => t('View for listing options'),
      '#description' => t('The selected view (and display) will be shown in a modal for users to select values for the target filter. The used Views display must include a "Views Bulk Operations" field.'),
      '#options' => $views_options,
      '#default_value' => $this->options['options_view'],
      '#required' => TRUE,
    );
    if (!$views_options) {
      drupal_set_message(t('No view with "Views Bulk Operations" field found. Cannot use this filter.'), 'error');
    }

    $filter_options = array();
    foreach ($this->view->display_handler->get_option('filters') as $filter_id => $filter) {
      if (!empty($filter['exposed']) && $filter_id != $this->options['id']) {
        if (!empty($filter['ui_name'])) {
          $label = $filter['ui_name'];
        }
        else {
          $definition = $views_data[$filter['table']][$filter['field']];
          $definition = $definition['filter'] + $definition;
          if (isset($views_data[$filter['table']]['table'])) {
            $definition += $views_data[$filter['table']]['table'];
          }
          $title = isset($definition['title short']) ? $definition['title short'] : $definition['title'];
          $label = t('!group: !title', array(
            '!group' => $definition['group'],
            '!title' => $title
          ));
        }
        $filter_options[$filter_id] = $label;
      }
    }
    asort($filter_options, SORT_NATURAL | SORT_FLAG_CASE);
    $form['filter'] = array(
      '#type' => 'select',
      '#title' => t('Target filter'),
      '#description' => t('The exposed filter for which values can be selected via the modal provided by this filter.'),
      '#options' => $filter_options,
      '#default_value' => $this->options['filter'],
      '#required' => TRUE,
    );
    if (!$filter_options) {
      drupal_set_message(t('No other exposed filter found. Add at least one other exposed filter to be able to set its values via a modal.'), 'error');
    }

    $id = $this->options['expose']['identifier'];
    if (!$id) {
      $id = $this->options['id'];
    }
    $base = $id;
    $i = 0;
    while (!$this->view->display_handler->is_identifier_unique($form_state['id'], $id)) {
      $id = $base . '_' . ++$i;
    }
    $form['expose']['identifier'] = array(
      '#type' => 'value',
      '#value' => $id,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function expose_form(&$form, &$form_state) {
    // Place our own "Button label" and "Modal title" fields right after the
    // initial "Label" field.
    $form['expose']['label'] = array();
    $form['expose']['button_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Button label'),
      '#description' => t('The label used for the button that adds the selected filter values from the modal popup.'),
      '#required' => TRUE,
      '#default_value' => $this->options['expose']['button_label'],
      '#size' => 40,
    );
    $form['expose']['modal_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Modal title'),
      '#description' => t('The title that will be displayed for the modal popup. Defaults to the link label.'),
      '#default_value' => $this->options['expose']['modal_title'],
      '#size' => 40,
    );

    parent::expose_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function admin_summary() {
    return check_plain($this->getLabel());
  }

  /**
   * {@inheritdoc}
   */
  public function can_build_group() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function is_exposed() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();

    $args['%filter'] = $this->ui_name(TRUE);
    if (empty($this->options['options_view'])) {
      $errors[] = t('Filter %filter: No view/display selected to display in modal.', $args);
    }
    if (empty($this->options['filter'])) {
      $errors[] = t('Filter %filter: No filter selected for which values should be selectable with modal.', $args);
    }
    else {
      $filters = $this->view->display_handler->get_option('filters');
      $filter_id = $this->options['filter'];
      if (empty($filters[$filter_id])) {
        $args['%target_filter'] = $filter_id;
        $errors[] = t('Filter %filter: Unknown filter with ID %target_filter set as target filter.', $args);
      }
      elseif (empty($filters[$filter_id]['exposed'])) {
        $args['%target_filter'] = $filters[$filter_id]['expose']['label'];
        $errors[] = t('Filter %filter: Selected target filter %target_filter is not exposed and therefore cannot be set via modal.', $args);
      }
    }

    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function value_form(&$form, &$form_state) {
    if (empty($form_state['exposed'])) {
      return;
    }
    list ($view_id, $display_id) = explode(':', $this->options['options_view'], 2);
    $modal_title = $this->options['expose']['modal_title'];
    if (!$modal_title) {
      $modal_title = $this->getLabel();
    }
    $link = array(
      '#type' => 'link',
      '#title' => $this->getLabel(),
      '#href' => 'views/ajax',
      '#options' => array(
        'query' => array(
          'view_name' => $view_id,
          'view_display_id' => $display_id,
        ),
        'attributes' => array(
          'class' => array('views-vbo-filter'),
          'id' => drupal_html_id($this->options['expose']['identifier']),
          'data-target-filter' => $this->view->filter[$this->options['filter']]->options['expose']['identifier'],
          'data-button-label' => $this->options['expose']['button_label'],
          'data-modal-title' => $modal_title,
        ),
      ),
    );
    $base_path = drupal_get_path('module', 'views_vbo_filter');
    $form['value'] = array(
      '#markup' => render($link),
      '#attached' => array(
        'js' => array($base_path . '/views_vbo_filter.js'),
        'css' => array($base_path . '/views_vbo_filter.css'),
      ),
      '#pre_render' => array(
        'drupal_pre_render_markup',
        'views_vbo_filter_include_ctools_modal',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function exposed_info() {
    $info = parent::exposed_info();
    $info['label'] = '';
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function accept_exposed_input($input) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {}

  /**
   * Retrieves the label to use for the modal link.
   *
   * @return string
   *   The label to use for the modal link.
   */
  protected function getLabel() {
    return $this->options['expose']['label'] ? $this->options['expose']['label'] : t('Add filter values');
  }

}
