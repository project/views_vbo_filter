<?php

/**
 * @file
 * Contains hook implementations for the Views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_vbo_filter_views_data_alter(&$data){
  $data['views']['views_vbo_filter'] = array(
    'title' => t('Filter values dialog'),
    'help' => t('Opens a modal dialog on the page showing another view that lets you select values for another filter.'),
    'filter' => array(
      'handler' => 'ViewsVboFilter',
    ),
  );
}

